module safebox (
    input [15:0] key,
    output wire [7:0] outputs [0:16]  // SV support needed
);

wire U1, U2, U3, U4, unlock;
wire [6:0] kb0, kb1, kb2, kb3, kb4, kb5, kb6, kb7, kb8, kb9, kb10, kb11, kb12, kb13, kb14, kb15, kb16;

assign kb0 = {key[12], key[4], key[14], key[7], key[2], key[6], key[1]};
assign kb1 = {key[15], key[5], key[13], key[0], key[10], key[9], key[9]};
assign kb2 = {key[8], key[12], key[13], key[4], key[14], key[1], key[0]};
assign kb3 = {key[10], key[11], key[7], key[9], key[6], key[8], key[2]};
assign kb4 = {key[12], key[3], key[11], key[10], key[4], key[15], key[0]};
assign kb5 = {key[5], key[1], key[7], key[15], key[7], key[9], key[4]};
assign kb6 = {key[2], key[8], key[3], key[13], key[12], key[10], key[5]};
assign kb7 = {key[0], key[1], key[11], key[6], key[5], key[0], key[2]};
assign kb8 = {key[15], key[8], key[11], key[4], key[10], key[1], key[12]};
assign kb9 = {key[3], key[14], key[7], key[6], key[12], key[10], key[1]};
assign kb10 = {key[3], key[0], key[13], key[6], key[11], key[7], key[9]};
assign kb11 = {key[14], key[15], key[8], key[4], key[1], key[13], key[14]};
assign kb12 = {key[10], key[8], key[7], key[9], key[4], key[11], key[6]};
assign kb13 = {key[15], key[12], key[13], key[3], key[7], key[11], key[6]};
assign kb14 = {key[14], key[12], key[15], key[1], key[9], key[5], key[10]};
assign kb15 = {key[2], key[4], key[14], key[9], key[5], key[13], key[1]};
assign kb16 = {key[15], key[8], key[3], key[11], key[0], key[4], key[12]};

// Instantiate logicbank modules
logicbank lb0  (.kb(kb0), .unlock(unlock), .bo(outputs[0]));
logicbank lb1  (.kb(kb1), .unlock(unlock), .bo(outputs[1]));
logicbank lb2  (.kb(kb2), .unlock(unlock), .bo(outputs[2]));
logicbank lb3  (.kb(kb3), .unlock(unlock), .bo(outputs[3]));
logicbank lb4  (.kb(kb4), .unlock(unlock), .bo(outputs[4]));
logicbank lb5  (.kb(kb5), .unlock(unlock), .bo(outputs[5]));
logicbank lb6  (.kb(kb6), .unlock(unlock), .bo(outputs[6]));
logicbank lb7  (.kb(kb7), .unlock(unlock), .bo(outputs[7]));
logicbank lb8  (.kb(kb8), .unlock(unlock), .bo(outputs[8]));
logicbank lb9  (.kb(kb9), .unlock(unlock), .bo(outputs[9]));
logicbank lb10 (.kb(kb10), .unlock(unlock), .bo(outputs[10]));
logicbank lb11 (.kb(kb11), .unlock(unlock), .bo(outputs[11]));
logicbank lb12 (.kb(kb12), .unlock(unlock), .bo(outputs[12]));
logicbank lb13 (.kb(kb13), .unlock(unlock), .bo(outputs[13]));
logicbank lb14 (.kb(kb14), .unlock(unlock), .bo(outputs[14]));
logicbank lb15 (.kb(kb15), .unlock(unlock), .bo(outputs[15]));
logicbank lb16 (.kb(kb16), .unlock(unlock), .bo(outputs[16]));

// unlock Logic
assign U1 = key[15] & ~key[14] & ~key[13] & key[12];
assign U2 = key[11] & key[10] & ~key[9] & key[8];
assign U3 = ~key[7] & key[6] & key[5] & ~key[4];
assign U4 = key[3] & key[2] & ~key[1] & key[0];
assign unlock = U1 & U2 & U3 & U4;

endmodule

module logicbank (
    input [6:0] kb,
    input unlock,
    output [7:0] bo
);

wire [7:0] bi;
assign bi = {1'b0, kb[6:0]};

assign bo = unlock ? bi : 8'b00000000;
endmodule
