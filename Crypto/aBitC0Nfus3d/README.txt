Difficulty: Medium
---------------------------------------------

A notorious hacking group, The Elite Nulls, has struck again! They claim to have deployed an "unbreakable" ransomware that encrypts files using AES, the gold standard in encryption. According to their bragging, they've gone the extra mile to secure their malware with a very strong 16-bit key.

Among the encrypted files, two stand out: a contact information list and a highly classified secret image. Paying the ransom of 0.420 DOGE is out of the question—your task is to outsmart their so-called security and recover the secret.

Can you break their "state-of-the-art" encryption and uncover what they’re hiding?
