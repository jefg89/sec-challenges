from flask import Flask, flash, redirect, render_template, request, session, abort
import hashlib
import string

def decrypt(encrypted, key):
	keylen = len(key)
	enclen = len(encrypted)
	out =  ""
	for i in range(0,enclen):
		try:
			t =   encrypted[i] ^ ord(key[i%keylen])
		except Exception as e:
			return ''
		out += chr(t)
		if t == 125:
			break

	return out



app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/auth", methods=['POST'])
def auth():
	password = request.form.get('password')
	try:
		encryptedPass = hashlib.sha256(password.encode('utf-8')).hexdigest()
	except Exception as e:
		return render_template('error.html')
	if encryptedPass == decrypt(shash, password):
		encrypted = [54,3,14,15,25,39,19,7,101,7,47,41,95,26,56,86,28]
		return render_template('admin.html', flag = decrypt(encrypted, password))
	else:
		return render_template('error.html')


			

if __name__ == "__main__":
	shash = "global"
	shash = [71,94,89,89,90,3,80,19,80,4,71,89,12,88,3,6,0,67,0,2,68,94,91,89,86,4,84,19,4,5,19,12,90,13,3,86,4,69,7,6,72,10,88,88,0,92,7,17,2,80,68,86,14,94,87,84,84,70,9,82,68,14,13,10]
	app.run(host='0.0.0.0')