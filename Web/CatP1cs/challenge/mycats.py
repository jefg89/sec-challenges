import os
from flask import Flask, request, send_file, render_template
import secrets

id_length = 8

BANNED = ["./", "..", "etc", "passwd","flag"]
app = Flask(__name__)

@app.route('/show', methods = ['GET'])
def show_picture():
	img = request.args.get('img') 
	for word in BANNED:
		if word in request.full_path:
			error = "What are you trying to do? >("
			return render_template('index.html', error = error)
	img_full_path = "static/img/" + img
	if not os.path.isfile(img_full_path):
		return render_template("404.html", path = img_full_path), 404
	else:
		return send_file(os.path.join(os.getcwd(), img_full_path))

@app.route('/', methods = ['POST'])
def reply():
	if (request.form.get("message")):
		toprint="Thx, your message id is: " + secrets.token_urlsafe(id_length)
		return render_template('index.html', error = toprint)
	
	return render_template('index.html', error = "")

@app.route('/')
def cat_picture():
	return render_template('index.html', error = "")



if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=1337)
