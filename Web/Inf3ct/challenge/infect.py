from flask import Flask, render_template, render_template_string, request
import jinja2

bad_chars = "{'_#&;}"
log_array = ['', '', '', '', '']

app = Flask(__name__)


def prepare(content):
	tmp = content
	for c in bad_chars:
		if c in tmp:
			tmp = tmp.replace(c,'')
	return tmp

@app.route("/", methods=['GET'])
def index():
    return render_template('index.html')

@app.route("/", methods=['POST'])
def post():
	msg = prepare(request.form.get('msg'))
	body = " ".join(["<div><b>Body:</b>",  msg, "</div>"])
	headers = " ".join(["<div><b>Headers:</b>", str(request.headers), "</div>"])	
	ip = " ".join(["<div><b>IP</b>:", request.remote_addr, "</div>"]) 
	log_data = ''.join([ip, headers, body])
	global log_array
	log_array = log_array[1:]
	log_array.append(log_data)
	return render_template("index.html", posted = f"Your secret is: '{msg}'")


@app.route("/log")
def logs():
	log_str = ''.join(log_array)
	# Template here
	template = '''
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset='utf-8'>
		<title>Logs</title>
		<h1>Recent request history</h1>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
	</head>
	<body>
		<p>''' + log_str + '''</p>
	</body>
	</html>'''

	return render_template_string(template)		
	
if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=1337)