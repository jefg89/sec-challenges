from flask import Flask, flash, redirect, render_template, request, session, abort
import hashlib
import string

def decrypt(encrypted, key):
	keylen = len(key)
	enclen = len(encrypted)
	out =  ""
	for i in range(0,enclen):
		try:
			t =   encrypted[i] ^ ord(key[i%keylen])
		except Exception as e:
			return ''
		out += chr(t)
		if t == 125:
			break

	return out



app = Flask(__name__)

@app.route("/", methods=['POST', 'GET'])
def index():
	if request.method == 'POST':
		password = request.form.get('answer')
		try:
			encryptedPass = hashlib.sha256(password.encode('utf-8')).hexdigest()
		except Exception as e:
			return render_template('index.html')
		if encryptedPass == decrypt(shash, password):
			encrypted = [114,94,85,85,79,117,0,94,0,74,77,79]
			return render_template('index.html', flag = decrypt(encrypted, password))
		else:
				return render_template('index.html')
	else:
		return render_template('index.html')


			

if __name__ == "__main__":
	shash = "global"
	shash = [3,1,0,5,1,81,86,6,4,83,1,4,12,87,12,86,85,10,85,2,0,7,87,87,80,3,5,2,5,1,3,87,5,7,13,84,12,11,4,83,87,6,80,83,12,10,7,80,2,80,5,5,80,81,2,7,5,80,7,83,12,2,0,11]
	app.run(host='0.0.0.0')